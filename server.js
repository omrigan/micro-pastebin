/* 
 * server.js
 * 
 * The main file, to be invoked at the command line. Calls app.js to get 
 * the Express app object.
 */

var app = require('./app').init(4000);
var pastebintxt="initialtext";
var data=new Object()
var locals = {
        title: 		 'Micro-pastebin',
        description: 'A Node.js applicaton bootstrap using Express 3.x, EJS, Twitter Bootstrap, and CSS3',
        author: 	 'Omrigan',
        _layoutFile: true
    };

app.get('/', function(req,res){

    locals.date = new Date().toLocaleDateString();

    res.render('mainhome.ejs', locals);
});
app.get('/about', function(req,res){
    res.render('about.ejs', locals);
});
app.post('/', function(req,res){
    pastebintxt = req.body.txt;
    res.send({code: 0})
});
app.get('/get', function(req,res){
   res.send({txt: pastebintxt})
});
app.post('/id:id', function(req, res){
    data[req.params.id]= req.body.txt;
    console.log(data)
    res.send({code: 0})
});
app.get('/id:id', function(req, res){
    locals.id=req.params.id.toString();
   res.render('home.ejs', locals)
});
app.get('/get:id', function(req, res){
    var txt =  data[req.params.id]
    if(txt==undefined){
        txt="Not stated yet"
    }
    res.send({txt: txt})
});
/* The 404 Route (ALWAYS Keep this as the last route) */
app.get('/*', function(req, res){
    res.render('404.ejs', locals);
});